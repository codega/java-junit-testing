package Luhn;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;



class LuhnsAlgorithmTest {
    @Test
    void checkLuhnTrue() {
        String cardString = "543534345243";
        LuhnsAlgorithm testLuhn = new LuhnsAlgorithm(cardString);
        assertTrue(testLuhn.checkLuhn());
    }

    @Test
    void checkLuhnFalse(){
        String cardString = "543534345249";
        LuhnsAlgorithm testLuhn = new LuhnsAlgorithm(cardString);
        assertFalse(testLuhn.checkLuhn());
    }

    @Test
    void getExpectedTrue() {
        String cardString = "543534345247";
        LuhnsAlgorithm testLuhn = new LuhnsAlgorithm(cardString);
        assertEquals(3, testLuhn.getExpected());
    }

    @Test
    void getExpectedFalse() {
        String cardString = "543534345247";
        LuhnsAlgorithm testLuhn = new LuhnsAlgorithm(cardString);
        assertNotEquals(7, testLuhn.getExpected());
    }

    @Test
    void lastProvidedTrue() {
        String cardString = "543534345247";
        LuhnsAlgorithm testLuhn = new LuhnsAlgorithm(cardString);
        assertEquals('7', testLuhn.lastProvided());

    }

    @Test
    void lastProvidedFalse() {
        String cardString = "543534345247";
        LuhnsAlgorithm testLuhn = new LuhnsAlgorithm(cardString);
        assertNotEquals('9', testLuhn.lastProvided());
    }

    @Test
    void providedInputTrue() {
        String cardString = "543534345247";
        LuhnsAlgorithm testLuhn = new LuhnsAlgorithm(cardString);
        assertEquals("54353434524 7", testLuhn.providedInput());
    }

    @Test
    void providedInputFalse() {
        String cardString = "543534345247";
        LuhnsAlgorithm testLuhn = new LuhnsAlgorithm(cardString);
        assertNotEquals("543534345247", testLuhn.providedInput());
    }

    @Test
    void numberLengthTrue() {
        String cardString = "5435343452474224";
        LuhnsAlgorithm testLuhn = new LuhnsAlgorithm(cardString);
        assertEquals(16, testLuhn.numberLength());
    }

    @Test
    void numberLengthFalse() {
        String cardString = "54353434524742";
        LuhnsAlgorithm testLuhn = new LuhnsAlgorithm(cardString);
        assertNotEquals(16, testLuhn.numberLength());
    }
}