import java.util.Scanner;

import Luhn.*;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Please enter a number to check: ");
        String tobeChecked = input.nextLine();

        LuhnsAlgorithm check = new LuhnsAlgorithm(tobeChecked);
        boolean checkResult = check.checkLuhn();

        System.out.println("Input: " + check.providedInput());

        System.out.println("Provided: " + check.lastProvided());

        System.out.println("Expected: " + check.getExpected());

        if (checkResult){
            System.out.println("Checksum: VALID!");
        } else {
            System.out.println("Checksum: NOT VALID!");
        }

        if (check.numberLength() == 16){
            System.out.println("Digits: 16 (Credit Card)");
        } else {
            System.out.println("Digits: " + check.numberLength() + " (Not a valid credit card!)");
        }

    }
}


