package Luhn;

public class LuhnsAlgorithm {
    public String cardNo;
    public int expected;

    public LuhnsAlgorithm(String cardNo){
        this.cardNo = cardNo;
        this.expected = 0;
        checkLuhn();
    }

    public boolean checkLuhn() {
        int digits = numberLength();
        String input = cardNo.substring(0, numberLength());
        boolean second = false;
        int sum = 0;

        for (int i = digits-1; i >= 0; i--) {
            int current = Character.getNumericValue(input.charAt(i));
            if (second) {
                current = current * 2;
            }
            if (current > 9) {
                String stringCurrent = Integer.toString(current);
                current = 0;
                for (int j = 0; j < stringCurrent.length(); j++){
                    current += Character.getNumericValue(stringCurrent.charAt(j));
                }
            }
            sum += current;
            second = !second;
        }

        int leftover = sum % 10;
        int intProvided = Character.getNumericValue(lastProvided());
        expected = (10 - leftover + intProvided)%10;

        return (sum % 10 == 0);
    }

    public int getExpected(){
        return this.expected;
    }

    public char lastProvided(){
        return cardNo.charAt(numberLength()-1);
    }

    public String providedInput(){
        String provided = cardNo.substring(0, numberLength()-1);
        char lastDigit = cardNo.charAt(numberLength()-1);
        return provided + " " + lastDigit;
    }

    public int numberLength(){
        return cardNo.length();
    }
}
