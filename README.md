# Java - Luhns Algorithm Testing

This application does a series of checks on a String inputted by the user. It uses Luhns Algorithm to calculate if the String is a valid credit card number.

## Getting Started

### Compile and run

To compile the application, do this command in the src-folder:
> javac \*.java luhn/\*.java

And then run the program by doing this command in the out folder:
> java Main

### Test program

Run tests by opening program in IntelliJ and going into the test classes, clicking run tests. You might have to download the Junit5-dependency before running the tests.